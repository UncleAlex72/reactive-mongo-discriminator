package uk.co.unclealex.discriminator

import org.scalactic.TypeCheckedTripleEquals
import org.scalatest.{Matchers, WordSpec}
import reactivemongo.bson.{BSON, BSONHandler, BSONReader, BSONString, BSONWriter}

import scala.util.Try

class BsonStringsSpec extends WordSpec with Matchers with TypeCheckedTripleEquals {

  sealed trait Bool {
    val token: String
  }

  object Bool {

    class BoolImpl(override val token: String) extends Bool

    object True extends BoolImpl("TRUE")
    object False extends BoolImpl("FALSE")

    val findByToken: String => Option[Bool] = token => Seq(True, False).find(_.token == token)
  }

  "A reader" should {
    implicit val reads: BSONReader[BSONString, Bool] = BsonStrings.reader(Bool.findByToken)
    "read a true" in {
      BSON.read[BSONString, Bool](BSONString("TRUE")) should ===(Bool.True)
    }
    "read a false" in {
      BSON.read[BSONString, Bool](BSONString("FALSE")) should ===(Bool.False)
    }
    "not read any other string" in {
      Try(BSON.read[BSONString, Bool](BSONString("HELLO"))).isSuccess should ===(false)
    }
  }

  "A writer" should {
    implicit val writes: BSONWriter[Bool, BSONString] = BsonStrings.writer(_.token)
    "write a true" in {
      BSON.write[Bool, BSONString](Bool.True) should === (BSONString("TRUE"))
    }
    "write a false" in {
      BSON.write[Bool, BSONString](Bool.False) should === (BSONString("FALSE"))
    }
  }

  "A handler" should {
    implicit val handler: BSONHandler[BSONString, Bool] = BsonStrings.handler(Bool.findByToken, _.token)
    "read a true" in {
      BSON.read[BSONString, Bool](BSONString("TRUE")) should ===(Bool.True)
    }
    "read a false" in {
      BSON.read[BSONString, Bool](BSONString("FALSE")) should ===(Bool.False)
    }
    "not read any other string" in {
      Try(BSON.read[BSONString, Bool](BSONString("HELLO"))).isSuccess should ===(false)
    }
    "write a true" in {
      BSON.write[Bool, BSONString](Bool.True) should === (BSONString("TRUE"))
    }
    "write a false" in {
      BSON.write[Bool, BSONString](Bool.False) should === (BSONString("FALSE"))
    }
  }
}
