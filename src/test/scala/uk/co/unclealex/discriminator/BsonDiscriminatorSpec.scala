package uk.co.unclealex.discriminator

import org.scalatest.{Matchers, WordSpec}
import reactivemongo.bson.{BSON, BSONDocument, BSONDocumentHandler, BSONDocumentReader, BSONDocumentWriter, BSONString, Macros}

import scala.util.Try

class BsonDiscriminatorSpec extends WordSpec with Matchers {

  sealed trait Choice
  case class FirstChoice(first: String) extends Choice
  case class SecondChoice(second: String) extends Choice

  val firstChoiceBSONDocumentWriters: BSONDocumentWriter[FirstChoice] = Macros.writer[FirstChoice]
  val secondChoiceBSONDocumentWriter: BSONDocumentWriter[SecondChoice] = Macros.writer[SecondChoice]
  val firstChoiceBSONDocumentReader: BSONDocumentReader[FirstChoice] = Macros.reader[FirstChoice]
  val secondChoiceBSONDocumentReader: BSONDocumentReader[SecondChoice] = Macros.reader[SecondChoice]

  "A reader" should {
    implicit val choiceBSONDocumentReader: BSONDocumentReader[Choice] = BsonDiscriminator.reader("type") {
      case "first" => firstChoiceBSONDocumentReader.read
      case "second" => secondChoiceBSONDocumentReader.read
    }
    "be able to read a first" in {
      BSON.readDocument[Choice](BSONDocument("type" -> "first", "first" -> "hello")) should ===(FirstChoice("hello"))
    }
    "be able to read a second" in {
      BSON.readDocument[Choice](BSONDocument("type" -> "second", "second" -> "hello")) should ===(SecondChoice("hello"))
    }
    "fail when there is no discriminator column" in {
      Try(BSON.readDocument[Choice](BSONDocument("first"-> "hello"))).isSuccess should ===(false)
    }
    "fail when there is an invalid discriminator column" in {
      Try(BSON.readDocument[Choice](BSONDocument("type" -> "dunno", "first"-> "hello"))).isSuccess should ===(false)
    }
    "fail when the discriminator column is not a string" in {
      Try(BSON.readDocument[Choice](BSONDocument("type" -> BSONDocument(), "first"-> "hello"))).isSuccess should ===(false)
    }
  }

  "A writer" should {
    implicit val choiceWrites: BSONDocumentWriter[Choice] = BsonDiscriminator.writer("type") {
      case fc: FirstChoice => "first" -> firstChoiceBSONDocumentWriters.write(fc)
      case sc: SecondChoice => "second" -> secondChoiceBSONDocumentWriter.write(sc)
    }
    "be able to write a first" in {
      BSON.writeDocument[Choice](FirstChoice("hello")).toMap should contain theSameElementsAs
        Map("type" -> BSONString("first"), "first" -> BSONString("hello"))
    }
    "be able to write a second" in {
      BSON.writeDocument[Choice](SecondChoice("hello")).toMap should contain theSameElementsAs
        Map("type" -> BSONString("second"), "second" -> BSONString("hello"))
    }
  }

  "A handler" should {
    implicit val choiceBSONDocumentHandler: BSONDocumentHandler[Choice] = BsonDiscriminator.handler("type") ({
      case "first" => firstChoiceBSONDocumentReader.read
      case "second" => secondChoiceBSONDocumentReader.read
    }, {
      case fc: FirstChoice => "first" -> firstChoiceBSONDocumentWriters.write(fc)
      case sc: SecondChoice => "second" -> secondChoiceBSONDocumentWriter.write(sc)
    })
    "be able to read a first" in {
      BSON.readDocument[Choice](BSONDocument("type" -> "first", "first" -> "hello")) should ===(FirstChoice("hello"))
    }
    "be able to read a second" in {
      BSON.readDocument[Choice](BSONDocument("type" -> "second", "second" -> "hello")) should ===(SecondChoice("hello"))
    }
    "fail when there is no discriminator column" in {
      Try(BSON.readDocument[Choice](BSONDocument("first"-> "hello"))).isSuccess should ===(false)
    }
    "fail when there is an invalid discriminator column" in {
      Try(BSON.readDocument[Choice](BSONDocument("type" -> "dunno", "first"-> "hello"))).isSuccess should ===(false)
    }
    "fail when the discriminator column is not a string" in {
      Try(BSON.readDocument[Choice](BSONDocument("type" -> BSONDocument(), "first"-> "hello"))).isSuccess should ===(false)
    }
    "be able to write a first" in {
      BSON.writeDocument[Choice](FirstChoice("hello")).toMap should contain theSameElementsAs
        Map("type" -> BSONString("first"), "first" -> BSONString("hello"))
    }
    "be able to write a second" in {
      BSON.writeDocument[Choice](SecondChoice("hello")).toMap should contain theSameElementsAs
        Map("type" -> BSONString("second"), "second" -> BSONString("hello"))
    }
  }
}
