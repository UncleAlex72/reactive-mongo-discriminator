package uk.co.unclealex.discriminator

import reactivemongo.bson.{BSONHandler, BSONReader, BSONString, BSONWriter}

/**
  * An object to make it easier to write BSON serialisers and deserialisers for sealed traits whose subclasses
  * can all be represented by BSON a string. For the rest of this documentation, assume the following code exists:
  * {{{
  *   import play.api.libs.json._
  *
  *   sealed trait Bool {
  *     val token: String
  *   }
  *
  *   object Bool {
  *
  *     class BoolImpl(override val token: String) extends Bool
  *
  *     object True extends BoolImpl("TRUE")
  *     object False extends BoolImpl("FALSE")
  *
  *     val findByToken: String => Option[Bool] = token => Seq(True, False).find(_.token == token)
  *   }
  * }}}
  */
object BsonStrings {

  /**
    * Generate an BSONWriter for a sealed trait. For example:
    * {{{
    *   implicit val writes: BSONWriter[Bool, BSONString] = BsonStrings.writer(_.token)
    * }}}
    *
    * @param tokenExtractor A function that turns the object into a token.
    * @tparam V The type of the object.
    * @return A writes that can write JSON objects.
    */
  def writer[V](tokenExtractor: V => String): BSONWriter[V, BSONString] = (v: V) => {
    BSONString(tokenExtractor(v))
  }

  /**
    * Generate an BSONReader for a sealed trait. For example:
    * {{{
    *   implicit val reads: BSONReader[BSONString, Bool] = BsonStrings.reader(Bool.findByToken)
    * }}}
    *
    * @param builder A function that will attempt to build an object from a string.
    * @tparam V The type of the object.
    * @return A reads that can read JSON objects.
    */
  def reader[V](builder: String => Option[V]): BSONReader[BSONString, V] = (bsonString: BSONString) => {
    val token = bsonString.value
    builder(token).getOrElse(throw new IllegalStateException(s"'$token' is not a valid token"))
  }

  /**
    * Generate a BSONHandler for a sealed trait. For example:
    * {{{
    *   implicit val handler: BSONHandler[BSONString, Bool] = BsonStrings.handler(Bool.findByToken, _.token)
    * }}}
    *
    * @param builder A function that will attempt to build an object from a string.
    * @param tokenExtractor A function that turns the object into a token.
    * @tparam V The type of the object.
    * @return A format that can read and write JSON objects.
    */
  def handler[V](builder: String => Option[V],
                tokenExtractor: V => String): BSONHandler[BSONString, V] = {
    val _reader = reader(builder).read _
    val _writer = writer(tokenExtractor).write _
    BSONHandler(_reader, _writer)
  }
}
