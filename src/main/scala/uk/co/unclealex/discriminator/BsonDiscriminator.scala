package uk.co.unclealex.discriminator

import reactivemongo.bson._

import scala.util.Try

/**
  * An object to make it easier to write BSON serialisers and deserialisers for sealed traits whose subclasses
  * can all be represented by BSON using a field whose value is unique for each subclass. For the documentation in
  * this class, assume the following have been defined:
  * {{{
  *   import reactivemongo.bson._
  *
  *   sealed trait Choice
  *   case class FirstChoice(first: String) extends Choice
  *   case class SecondChoice(second: String) extends Choice
  *
  *   val firstChoiceBSONDocumentWriters: BSONDocumentWriter[FirstChoice] = Macros.writer[FirstChoice]
  *   val secondChoiceBSONDocumentWriter: BSONDocumentWriter[SecondChoice] = Macros.writer[SecondChoice]
  *   val firstChoiceBSONDocumentReader: BSONDocumentReader[FirstChoice] = Macros.reader[FirstChoice]
  *   val secondChoiceBSONDocumentReader: BSONDocumentReader[SecondChoice] = Macros.reader[SecondChoice]
  * }}}
  */
object BsonDiscriminator {

  /**
    * Generate an BSONDocumentWriter for a sealed trait. For example:
    * {{{
    *   implicit val choiceBSONDocumentReader: BSONDocumentReader[Choice] = BsonDiscriminator.reader("type") {
    *     case "first" => firstChoiceBSONDocumentReader.read
    *     case "second" => secondChoiceBSONDocumentReader.read
    *   }
    * }}}
    *
    * @param discriminatorFieldName The name of the field to be used as a discriminator.
    * @param writerBuilder A builder that builds a writer given the type of the object being written.
    * @tparam V The type of object being written.
    * @return A writer that will write the object as BSON and add a discriminator field.
    */
  def writer[V](discriminatorFieldName: String)(writerBuilder: V => (String, BSONDocument)): BSONDocumentWriter[V] = (v: V) => {
    val (discriminatorFieldValue, bsonDocument) = writerBuilder(v)
    bsonDocument ++ (discriminatorFieldName -> BSONString(discriminatorFieldValue))
  }

  /**
    * Generate a BSONDocumentReader for a sealed trait. For example:
    * {{{
    *   implicit val choiceBSONDocumentReader: BSONDocumentReader[Choice] = BsonDiscriminator.reader("type") {
    *     case "first" => firstChoiceBSONDocumentReader.read
    *     case "second" => secondChoiceBSONDocumentReader.read
    *   }
    * }}}
    *
    * @param discriminatorFieldName The name of the field to be used as a discriminator.
    * @param readerBuilder A function that takes a string and returns a function that reader a BSON object and returns a
    *                     JsResult.
    * @tparam V The type of the object to be read.
    * @return A Reader that reader a BSON object and looks at the value of a discriminator field to work out
    *         which type of object should be read.
    */
  def reader[V](discriminatorFieldName: String)
              (readerBuilder: PartialFunction[String, BSONDocument => V]): BSONDocumentReader[V] = new BSONDocumentReader[V] {
    override def read(bson: BSONDocument): V = {
      def fullReaderBuilder(discriminatorColumnValue: String): Try[BSONDocument => V] = {
        readerBuilder.lift(discriminatorColumnValue).toRight(
          new IllegalStateException(s"$discriminatorColumnValue is not a valid discriminator value.")).toTry
      }
      val tryValue: Try[V] = for {
        discriminatorColumnValue <- bson.getAsTry[String](discriminatorFieldName)
        reader <- fullReaderBuilder(discriminatorColumnValue)
      } yield {
        reader(bson)
      }
      tryValue.get
    }
  }

  /**
    * Generate a BSONDocumentHandler for a sealed trait. For example:
    * {{{
    *   implicit val choiceBSONDocumentHandler: BSONDocumentHandler[Choice] = BsonDiscriminator.format("type") ({
    *     case "first" => firstChoiceBSONDocumentReader.read
    *     case "second" => secondChoiceBSONDocumentReader.read
    *   }, {
    *     case fc: FirstChoice => "first" -> firstChoiceBSONDocumentWriters.write(fc)
    *     case sc: SecondChoice => "second" -> secondChoiceBSONDocumentWriter.write(sc)
    *   })
    * }}}
    *
    * @param discriminatorFieldName The name of the field to be used as a discriminator.
    * @param readerBuilder A function that takes a string and returns a function that reader a BSON object and returns a
    *                     JsResult.
    * @param writerBuilder A builder that builds a writer given the type of the object being written.
    * @tparam V The type of object being written.
    * @return A format that combines a reader and writer.
    */
  def handler[V](discriminatorFieldName: String)
               (readerBuilder: PartialFunction[String, BSONDocument => V],
                writerBuilder: V => (String, BSONDocument)): BSONDocumentHandler[V] = {
    val _reader = reader(discriminatorFieldName)(readerBuilder).read _
    val _writer = writer(discriminatorFieldName)(writerBuilder).write _
    BSONDocumentHandler(_reader, _writer)
  }
}
