import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._
import sbt.Keys._
import sbt._

scalaVersion     := "2.13.0"
organization     := "uk.co.unclealex"
organizationName := "UncleAlex"

name := "reactive-mongo-discriminator"
libraryDependencies ++= Seq(
  "org.reactivemongo" %% "reactivemongo-bson-macros" % "0.18.4",
  "org.scalatest" %% "scalatest" % "3.0.8" % Test)

releaseProcess := Seq[ReleaseStep](
  checkSnapshotDependencies, // : ReleaseStep
  inquireVersions, // : ReleaseStep
  runTest, // : ReleaseStep
  setReleaseVersion, // : ReleaseStep
  commitReleaseVersion, // : ReleaseStep, performs the initial git checks
  tagRelease, // : ReleaseStep
  releaseStepCommand("publishLocal"),
  publishArtifacts,
  setNextVersion, // : ReleaseStep
  commitNextVersion, // : ReleaseStep
  pushChanges // : ReleaseStep, also checks that an upstream branch is properly configured
)
